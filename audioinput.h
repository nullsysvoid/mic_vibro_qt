#ifndef AUDIOINPUT_H
#define AUDIOINPUT_H

#include <QObject>
#include <QAudioInput>
#include <QFile>
#include <QTimer>
#include <QAudioRecorder>
#include <QUrl>
#include <QAudioProbe>
#include <QDebug>

class AudioInput : public QObject{

    Q_OBJECT
    public Q_SLOTS:
        void terminateRecording();
        void processBuffer(const QAudioBuffer&);


public:
    void setup();

 private:
    QAudioRecorder *audioRecorder;
    QAudioProbe *probe;
};

#endif // AUDIOINPUT_H
