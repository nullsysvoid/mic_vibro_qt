QT += core
QT += multimedia
QT -= gui

CONFIG += c++11

TARGET = untitled4
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    audioinput.cpp

HEADERS += \
    audioinput.h
